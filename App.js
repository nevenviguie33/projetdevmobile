import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Confinement from './Confinement';
import Home from './Home';
import CouvreFeu from './CouvreFeu';
import { enableScreens } from 'react-native-screens';

import { LogBox } from 'react-native';

import * as Location from 'expo-location';



enableScreens();

import { NavigationContainer } from '@react-navigation/native';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';

let latUser;
let longUser;

const Stack = createNativeStackNavigator()

export default function App() {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect(() => {
      (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
          setErrorMsg('Permission to access location was denied');
          return;
      }

      let location = await Location.getCurrentPositionAsync({LocationAccuracy: 4});
      setLocation(location);
      })();
  }, []);

  let text = 'Waiting..';
  if (errorMsg) {
      text = errorMsg;
  } else if (location) {
      text = JSON.stringify(location.latitude);
      latUser=location.coords.latitude;
      longUser=location.coords.longitude;
      
      
  }
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
          options={{headerLargeTitle: true}}
          name='Alerte Covid' 
          component={Home}/>
        <Stack.Screen
          options={{headerTitleStyle:true}}
          name='Confinement'
          component={Confinement}/>
        <Stack.Screen
          options={{headerTitleStyle:true}}
          name='Couvre Feu'
          component={CouvreFeu}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'dodgerblue',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS==="android" ? StatusBar.currentHeight : 0
  },
});

LogBox.ignoreLogs([
  'Animated: `useNativeDriver` was not specified.',
]);

export {latUser};
export {longUser};