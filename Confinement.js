import axios from 'axios';
import React,{useState,Component} from 'react';
import { StyleSheet, Text, View, TextInput, Button, ImageBackground, Alert } from 'react-native';
import * as Location from 'expo-location';


import {latUser} from './App';
import {longUser} from './App';

let latAdr=50.461338;
let longAdr=3.531503;



export default class Confinement extends React.Component {
    constructor(props){
        super(props)
        
        this.state = {
            adresse: '',
            errorMessage: '',
            location: {},
            d: 0,
            message: ''
        }
    }
    _handlePress() {
        var adr=this.state.adresse;
        geocode(adr);
    }

    
    _getDistance(){
        const d=Distance(latAdr,longAdr,latUser,longUser);
        this.setState({
            d
        });
        if(d>10000){
            const message = 'Vous dépassez la zone de confinement de 10 km !'
            this.setState({
                message
            });
            Alert.alert("Attention","Vous dépassez la distance de 10km !",
            [
                { text: "OK" }
              ])
        }
    }
    //https://afficheo.com/wp-content/uploads/2019/09/d%C3%A9tail-affiche-d%C3%A9co-paris-noir-et-blanc-afficheo.jpg
    render(){
        return (
            <ImageBackground blurRadius={0.6} source={{
                uri: 'https://static.vecteezy.com/ti/vecteur-libre/p1/625953-fond-abstrait-doux-dans-des-couleurs-claires-vectoriel.jpg'}} style={styles.background} >
                
                <Text style={styles.texte3}>Votre distance actuelle :</Text>
                
                <Text style={styles.texte}>Entrez votre adresse pour être informé en cas de dépassement de la distance maximale autorisée :</Text>
                
                <TextInput 
                    style={styles.input}
                    placeholder='Votre Adresse'
                    returnKeyLabel = {"next"}
                    onChangeText={(text) => this.setState({adresse:text})}
                />
                <Button title="Valider" style={styles.button} onPress={() => this._handlePress()}/>
                <Text style={styles.texte2}>Votre adresse est : {this.state.adresse}</Text>
                <Button style={styles.button1} title="Distance"  onPress={()=> this._getDistance()}/>
                <Text style={styles.distance}>{this.state.d} m</Text>
                <Text style={styles.message}>{this.state.message}</Text>
                
            </ImageBackground>  
            
        )
    }
}



    



//Obtention des coordonnées GPS de l'adresse
function geocode(addresse){
    axios.get('http://api.positionstack.com/v1/forward',{
        params:{
            access_key  :'e00b10da5486e6863d868df847cb0cd2',
            query:addresse,      
        }
    })
    .then(function(response){
        longAdr=response.data.data[0].longitude
        latAdr=response.data.data[0].latitude
        console.log(longAdr,latAdr);
    })
    .catch(function(error){
        console.log(error);
    })
}

/* Impossible d'executer ce code dans une fonction et donc de lier l'obtention de la position à un bouton

function Geolocalisation() {
    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);

    useEffect(() => {
        (async () => {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
        }

        let location = await Location.getCurrentPositionAsync({LocationAccuracy: 4});
        setLocation(location);
        })();
    }, []);

    let text = 'Waiting..';
    if (errorMsg) {
        text = errorMsg;
    } else if (location) {
        text = JSON.stringify(location);
        console.log(location);
    }
}
*/

//Calcul de la distance entre adresse et position actuelle

//Conversion des degrés en radian
function convertRad(input){
    return (Math.PI * input)/180;
}

function Distance(lat_a_degre, lon_a_degre, lat_b_degre, lon_b_degre){
 
    let R = 6378000 //Rayon de la terre en mètre
    
    let lat_a = convertRad(lat_a_degre);
    let lon_a = convertRad(lon_a_degre);
    let lat_b = convertRad(lat_b_degre);
    let lon_b = convertRad(lon_b_degre);
    
    let d = R * (Math.acos(Math.sin(lat_a)*Math.sin(lat_b) + Math.cos(lat_a)*Math.cos(lat_b)*Math.cos(lon_b - lon_a)))
    return Math.trunc(d);
}
//(Math.PI/2 - Math.asin( Math.sin(lat_b) * Math.sin(lat_a) + Math.cos(lon_b - lon_a) * Math.cos(lat_b) * Math.cos(lat_a)))

//Pour IOS : ajouter les flex en commentaire
const styles = StyleSheet.create({
    background: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
    },
    
    bandeauBottom: {
        backgroundColor: "dodgerblue",
        width: '100%',
        height: 60,
        position :'absolute',
        bottom: 0
    },
    titres: {
        flex :1,
        fontFamily: "monospace",
        fontSize: 22,
        fontWeight: 'bold',
        position: 'absolute',
        top: 50,
        left: 20
    },
    texte: {
        
        fontSize: 19,
        fontWeight: 'normal',
        top: 30,
        left: 15       
    },
    texte2: {
        flex: 1 ,
        fontSize: 15,
        fontWeight: 'normal',
        top: 20,       
    },
    texte3: {
        flex :1,
        fontSize: 19,
        fontWeight: 'normal',
        position: 'absolute',
        top: 430,      
    },
    texte4: {
        flex :1,
        fontSize: 19,
        fontWeight: 'normal',
        position: 'absolute',
        top: 530,      
    },
    input: {
        borderWidth: 1,
        borderColor: '#777',
        padding: 15,
        margin: 45,
        width: 330
    },
    button: {
        flex: 1,
        top: 10,
        margin: 10
    },
    button1: {
        flex:1,
        top: 100,
        margin: 10
    },
    distance: {
        flex :2,
        fontSize: 30,
        top: 100,
        height: 50
    },
    message: {
        flex :1,
        fontSize: 18,
        color: 'red',
        left : 10,
        top: 580,
        position: 'absolute'
    }
})

