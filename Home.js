import { NavigationContainer } from '@react-navigation/native';
import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Button, ImageBackground } from 'react-native';
import RoundedButton from './RoundedButton';

export default class Home extends Component{
    constructor(props){
        super(props)

        this.state={
            
        }
    }

    goToConfinementPage(){
        this.props.navigation.navigate("Confinement")
    }
    goToCouvreFeuPage(){
        this.props.navigation.navigate("Couvre Feu")
    }
 

    state = {
        data: []
      };
    
      componentDidMount() {
        this.fetchData();
      }
    
      fetchData = async () => {
        
        const response = await fetch("https://coronavirusapi-france.vercel.app/FranceLiveGlobalData");
        const json = await response.json();
        this.setState({ data: json.FranceGlobalLiveData });
      };

    render(){
        return (
            

                <ImageBackground blurRadius={0.6} source={{uri: 'https://static.vecteezy.com/ti/vecteur-libre/p1/625953-fond-abstrait-doux-dans-des-couleurs-claires-vectoriel.jpg'}} style={{ flex: 5}}>
                    <View style={{ flex: 5}}>
                        <View style={{ flex: 1}}>
                            <View style={{ flex: 1, flexDirection: 'row'}}>
                                <View style={{ flex: 1}}><RoundedButton text={`CONFINEMENT
                                                                                Alerte distance`}color='dodgerblue' onPress={this.goToConfinementPage.bind(this)}/></View>
                                <View style={{ flex: 1}}><RoundedButton text={`COUVRE-FEU
                                                                                Alerte horaire`}color='dodgerblue'onPress={this.goToCouvreFeuPage.bind(this)}/></View>
                            </View>
                            </View>
                        <View style={{ flex: 4}}>
                            <Text style={styles.announce}>  {`Evolution du coronavirus AUJOURD'HUI`}</Text>
                            <FlatList
                            style={{width: '100%'}}
                                data={this.state.data}
                                keyExtractor={(item, index) => 'list-item-${index}'}
                                renderItem={({ item }) =>
                                <View>
                                    <View style={styles.row}>
                                        <Text style={styles.textNumbers}>Hospitalisés</Text> 
                                        <Text style={styles.numbers}>  {`${item.hospitalises}`}</Text>                               
                                    </View>
                                    <View style={styles.row}>                              
                                        <Text style={styles.textNumbers}>Nouvelles hospitalisations</Text> 
                                        <Text style={styles.numbers}>  {`${item.nouvellesHospitalisations}`}</Text> 
                                    </View>  
                                    <View style={styles.row}>
                                        <Text style={styles.textNumbers}>Réanimations</Text> 
                                        <Text style={styles.numbers}>  {`${item.reanimation}`}</Text> 
                                    </View>
                                    <View style={styles.row}>  
                                        <Text style={styles.textNumbers}>Nouvelles réanimations</Text> 
                                        <Text style={styles.numbers}>  {`${item.nouvellesReanimations}`}</Text> 
                                    </View>
                                    <View style={styles.row}>
                                        <Text style={styles.textNumbers}>Décès</Text> 
                                        <Text style={styles.numbers}>  {`${item.deces}`}</Text> 
                                    </View>
                                    <View style={styles.row}>
                                        <Text style={styles.textNumbers}>Guérisons</Text> 
                                        <Text style={styles.numbers}>  {`${item.gueris}`}</Text> 
                                    </View>
                                </View>}
    
                            />
                        </View>
                    </View>
            </ImageBackground>
        )
    }
}



const styles = StyleSheet.create({
    view: {
        flexDirection:'row',
        backgroundColor: 'cornflowerblue',
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    text: {
        fontFamily:'Arial',
        fontWeight:'bold',
        color: "white",
        fontSize: 20
    },
    numbers: {
        lineHeight:20,
        fontSize:20,
        fontWeight:'bold',
      },
      announce: {
        lineHeight:20,
        fontSize:20,
        fontWeight:'bold',
        fontStyle:'italic'
      },
    textNumbers: {
        lineHeight:20,
        fontSize:15,
      },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 25,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'white'

    },

});


