import React from 'react';
import { StyleSheet,TouchableOpacity,Text, View,Alert,Image, ImageBackground} from 'react-native' ;
import moment from 'moment';
import TimePicker from "react-native-24h-timepicker";
import image from './assets/img.png';


class CouvreFeu extends React.Component {
    constructor(props){
        super(props);
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            show:true,
            mode:'time',
            time: '',
            jour:moment().format('L'),
            estEnViolation:false,
            dateLimite:'',
            remainingTime:''
            
            

        };

    }

    onCancel() {
        this.TimePicker.close();}

    onConfirm(hour, minute) {
        let dateActuelle=new Date();
        let dateLim=new Date().setHours(hour,minute);
        this.setState({ time: `${hour}:${minute}` });
        this.setState({ dateLimite: dateLim});
        this.TimePicker.close();
        if (dateActuelle>dateLim){
            this.setState({estEnViolation:true});
        }
        else{
            this.setState({estEnViolation:false});
        }
        
        }
   
    render() {
        const chiffres = Array.from(Array(61).keys())
        let button;
        if (!this.state.estEnViolation && this.state.time!=''){
            button = <Text style={styles.textBon}>Il est encore autorisé de circuler jusque {this.state.time} .</Text> ;
        }
        if (this.state.estEnViolation && this.state.time!=''){
            button =<Text style={styles.textMauvais}>Vous êtes en infraction ! Vous vous exposez à une amende de 135 euros.</Text>;
        }
        if(this.state.time==''){
            button =<Text style={styles.textBlanc}>Texte blanc, blanc, blanc.</Text>;
        }
    


        return (
          <ImageBackground blurRadius={0.6} source= {{uri: 'https://static.vecteezy.com/ti/vecteur-libre/p1/625953-fond-abstrait-doux-dans-des-couleurs-claires-vectoriel.jpg?fbclid=IwAR33GsG6mO1UwrC8qQj-zlMQy0vCurvpG5Uuv59l07DezaDYi53waVzbFSI'}}
          style={styles.page}> 
            <Image
            style={styles.img}
            source={image}
            />
            <Text style={styles.text}>Journée du {this.state.jour}.{'\n'}
            Vérifiez si vous êtes en violation du couvre feu là où vous vous trouvez:</Text>
        
            <View style={styles.timefield}>
                <TouchableOpacity
                     onPress={() => this.TimePicker.open()}
                     style={styles.button}>
                    <Text style={styles.buttonText}>Inscrire l'heure du couvre feu</Text>
                </TouchableOpacity>
                <Text style={styles.text}>{this.state.time}</Text>
                <TimePicker
                    ref={ref => { this.TimePicker = ref; }}
                    onCancel={() => this.onCancel()}
                    onConfirm={(hour, minute) => {this.onConfirm(hour, minute);}}/>
            </View>
            {button}
            </ImageBackground>

        )
      }
    }


const styles = StyleSheet.create({
    img: {
        flex:1.5,
        transform:[{ scale: 3/4 }],
        width: 550,
        alignSelf:'center'
      },
    page :{
        flex:1
    },
    timefield :{
        flex:1
    },
    text: {
      color: "black",
      fontSize: 25,
      alignSelf:'center'
      
    },
    textBon: {
        flex:1,
        color: "green",
        fontSize: 30,
        alignSelf:'center'
        
      },
    textMauvais: {
        flex:1,
        color: "red",
        fontSize: 30,
        alignSelf:'center'
        
      },
      textBlanc: {
        flex:1,
        color: 'transparent',
        fontSize: 30,
        alignSelf:'center'
        
      },
      button: {
        backgroundColor: 'dodgerblue',
        paddingVertical: 11,
        paddingHorizontal: 17,
        borderRadius: 3,
        marginVertical: 50
      },
      buttonText: {
        color: "#FFFFFF",
        fontSize: 16,
        fontWeight: "600",
        alignSelf:'center'
      }


})

export default CouvreFeu
