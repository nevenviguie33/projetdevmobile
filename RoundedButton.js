import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const RoundedButton = props => {
    const content = (
        <View style = {[styles.button, {backgroundColor:props.color} ]}>
            <Text style = {styles.text}>{props.text}</Text>
        </View>
    )
    return<TouchableOpacity onPress ={props.onPress}>{content}</TouchableOpacity>
}

const styles = StyleSheet.create({
    
    button: {
        padding:16,
        borderRadius: 14,
        alignItems:'center',
        marginTop:10,
        marginLeft:7,
        marginRight:7,
        
        
        
    },
    text: {
        color: "white",
        fontSize: 18,
        textAlign:'center'
        
      },

});

export default RoundedButton
